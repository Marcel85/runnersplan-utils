# runnersplan-utils

_utils package of the runnersplan project_

Some helpful coding for projects using Django, Bootstrap 3 and crispy-forms:

* DeleteView, DetailView and ListView classes inherits form Django's generic view classes and enables user restriction.

* ModelForm class inherits form Django's ModelForm class, set FormHelper configurations using meta data, initializes localizations and more. Helpful when using crispy-forms.

* Color field class for fields with hex color values.

* IntegerRangeField: Model integer field class with minimum and maximum value.

* Some template tags and filters: brightness, carousel_item, contains, link_scripts, link_stylesheets, month_name, …

* Some widget classes: datepicker_widget (needs bootstrap-datepicker), colorpalette_widget (needs bootstrap-colorpalette), typeahead_widget (needs Twitters Typeahead), summernote_widget (needs Summernote), switch_widget (needs bootstrap-switch)