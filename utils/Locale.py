# -*- coding: utf-8 -*-
import locale

class Locale(object):
    """ Class with static methods for localization. """

    def set_locale(category, locale_value):
        """ Set localization. """
        try:
            # try normal setting
            locale.setlocale(category, locale_value)
        except:
            try:
                # try setting with utf8 extension
                locale.setlocale(category, locale_value + '.utf8')
            except:
                pass
    set_locale = staticmethod(set_locale)