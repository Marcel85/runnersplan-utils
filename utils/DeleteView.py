# -*- coding: utf-8 -*-
from django.views.generic.edit import DeleteView
from django.db.models.deletion import ProtectedError
from django.http.response import HttpResponseRedirect

class DeleteView(DeleteView):
    """ 
    DeleteView class inherits form Django's DeleteView class and
    enables user restriction.
    """
    
    user_restriction = False 
    protected_error_url = ''

    def get_queryset(self):
        """ 
        Returns the queryset that will be used to retrieve the object that
        the view will display, if necessary with user restriction.
        """
        queryset = super(DeleteView, self).get_queryset()
        if self.user_restriction:
            return queryset.filter(user=self.request.user)
        else:
            return queryset
        
    def post(self, request, *args, **kwargs):
        """
        Handles PROTECTED ERROR on delete.
        """
        try:
            return self.delete(request, *args, **kwargs)
        except ProtectedError:
            return HttpResponseRedirect(self.protected_error_url)