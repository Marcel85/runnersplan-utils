# -*- coding: utf-8 -*-

class Calendar(object):
    """ Class with static calendar methods. """

    def aggregate_by_ymd(records, date_field, aggregate_field='count'):
        """
        Aggregates a numeric field in a record set by year, month and day. 
        If the aggregate field is not specified, the records will be counted.
        """
        stats = []
        years = []
        for record in records:
            date = record[date_field]
            year = date.year
            month = date.month
            day = date.day
            if aggregate_field=='count':
                number = 1
            else:
                number = record[aggregate_field]
            if not year in years:
                years.append(year)
                months = []
                year_dict = {'year': year, 'month': [], aggregate_field: 0}
                stats.append(year_dict)
            if not month in months:
                months.append(month)
                days = []
                month_dict = {'month': month, 'day': [], aggregate_field: 0}
                year_dict['month'].append(month_dict)
            if not day in days:
                days.append(day)
                day_dict = {'day': day, aggregate_field: 0}
                month_dict['day'].append(day_dict)
            year_dict[aggregate_field] += number
            month_dict[aggregate_field] += number
            day_dict[aggregate_field] += number
        return stats
    aggregate_by_ymd = staticmethod(aggregate_by_ymd)