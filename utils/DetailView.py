# -*- coding: utf-8 -*-
from django.views.generic.detail import DetailView

class DetailView(DetailView):
    """ 
    DetailView class inherits form Django's DetailView class and
    enables user restriction.
    """
    
    user_restriction = False 

    def get_queryset(self):
        """ 
        Returns the queryset that will be used to retrieve the object that
        the view will display, if necessary with user restriction.
        """
        queryset = super(DetailView, self).get_queryset()
        if self.user_restriction:
            return queryset.filter(user=self.request.user)
        else:
            return queryset