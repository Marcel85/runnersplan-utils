# -*- coding: utf-8 -*-
class Color(object):
    """ Class with methods for handling colors. """ 
    
    def hex_to_rgb(value):
        """ Returns the rgb tuple of an hex color value. """
        try:
            value = value.lstrip('#')
            lv = len(value)
            return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))
        except:
            None
    hex_to_rgb = staticmethod(hex_to_rgb)
    
    def get_brightness(value):
        """ Returns the brightness value of a color as rgb tuple. """
        r, g, b = value
        return (r / 255.0) * 0.299 + (g / 255.0) * 0.587 + (b / 255.0) * 0.114
    get_brightness = staticmethod(get_brightness)
    
    def get_contrast_class(value):
        """ Returns the contrast class according to an hex color value. """
        try:
            if Color.get_brightness(Color.hex_to_rgb(value)) > 0.5:
                return 'dark'
            else:
                return 'light'
        except:
            return 'neutral'
    get_contrast_class = staticmethod(get_contrast_class)