# -*- coding: utf-8 -*-
from django.views.generic.list import ListView

class ListView(ListView):
    """ 
    ListView class inherits form Django's ListView class and
    enables user restriction.
    """
    
    user_restriction = False 

    def get_queryset(self):
        """ 
        Returns the queryset that will be used to retrieve the object that
        the view will display, if necessary with user restriction.
        """
        queryset = super(ListView, self).get_queryset()
        if self.user_restriction:
            return queryset.filter(user=self.request.user)
        else:
            return queryset