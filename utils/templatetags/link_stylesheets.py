# -*- coding: utf-8 -*-
from django import template
register = template.Library()

@register.tag('link_stylesheets')
def link_stylesheets(parser, token):
    """ Tag for linking CSS-files in HTML. """
    
    # read given attribute or set default
    try:
        media = token.split_contents()[1]
    except:
        media = 'screen'
        
    # parse node list
    nodelist = parser.parse(('endlink_stylesheets',))
    parser.delete_first_token() 
    return LinkStylesheetsNode(media, nodelist)

class LinkStylesheetsNode(template.Node):
    """ Node class of link_stylesheets template tag. """
    def __init__(self, media, nodelist):
        self.media = media
        self.nodelist = nodelist

    def render(self, context):
        html = ''
        for href in [lines.strip() for lines 
                     in self.nodelist.render(context).splitlines() 
                     if lines.strip() != '']:
            html += '<link href="{href}" rel="stylesheet" media="{media}">\n'.\
                format(href=href, media=self.media)
        return html