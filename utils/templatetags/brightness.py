# -*- coding: utf-8 -*-
from django import template
from runnersplan.utils.Color import Color
register = template.Library()

@register.filter()
def brightness(value):
    """
    Filter to get the brightness of an color as rgb tuple.
    Usage: {% if variable|brightness < 50 %}{% endif %}
    """
    return Color.get_brightness(value)