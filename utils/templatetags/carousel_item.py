# -*- coding: utf-8 -*-
from django import template
from django.template import Context, loader 
from runnersplan import settings
register = template.Library()

@register.simple_tag(takes_context=True)
def carousel_item(context, src, caption, active=False):
    """ Renders an item for a carousel. """
    template = loader.get_template('utils/templatetags/carousel_item.html') 
    context = Context({'user' : context['user'],
                       'src' : settings.STATIC_URL + src, 
                       'caption' : caption, 
                       'active' : active})
    return template.render(context)