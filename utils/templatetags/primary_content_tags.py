# -*- coding: utf-8 -*-
from django import template
from django.template import Context, loader 
from runnersplan.workout.models import WorkoutContentTags
register = template.Library()

@register.simple_tag
def primary_content_tags(workout):
    """ Returns the primary content tags of a workout. """
    # read primary tags of the workout (limit of 5 results)
    tags = WorkoutContentTags.objects.\
        filter(workout=workout, tag__primary=True)[:5]
        
    # rendering HTML of tags
    template = loader.get_template('utils/templatetags/primary_content_tags.html') 
    context = Context({'tags' : tags})
    return template.render(context)