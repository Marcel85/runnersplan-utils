# -*- coding: utf-8 -*-
from django import template
register = template.Library()

@register.tag('link_scripts')
def link_scripts(parser, token):
    """ Tag for linking JavaScript-files in HTML. """
    nodelist = parser.parse(('endlink_scripts',))
    parser.delete_first_token()  
    return LinkScriptsNode(nodelist)

class LinkScriptsNode(template.Node):
    """ Node class of link_scripts template tag. """
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        html = ''
        for url in [lines.strip() for lines 
                     in self.nodelist.render(context).splitlines() 
                     if lines.strip() != '']:
            html += '<script src="{url}"></script>\n'.format(url=url)
        return html