# -*- coding: utf-8 -*-
from django import template
register = template.Library()

@register.filter()
def contains(value, arg):
    """
    Filter to check if string contains substring.
    Usage: {% if variable|contains:"substring" %}{% endif %}
    """
    return str(arg) in str(value)