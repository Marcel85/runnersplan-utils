# -*- coding: utf-8 -*-
from django import template
from runnersplan.workout.models import Workout
register = template.Library()

@register.simple_tag
def part_of_day_text(identifier):
    """ Returns the text of a part of a day. """
    for opt in Workout.PART_OF_DAY_CHOICES:
        if opt[0] == identifier:
            return opt[1]