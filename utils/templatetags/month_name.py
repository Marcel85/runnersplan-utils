# -*- coding: utf-8 -*-
from django import template
import calendar, locale
from runnersplan.utils.Locale import Locale
register = template.Library()

@register.filter()
def month_name(month_number):
    """ Filter to get the month name by number. """
    Locale.set_locale(locale.LC_ALL, 'de_DE')
    return calendar.month_name[month_number]