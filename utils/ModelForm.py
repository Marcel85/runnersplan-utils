# -*- coding: utf-8 -*-
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field
from crispy_forms.bootstrap import FormActions, PrependedAppendedText

class ModelForm(forms.ModelForm):
    """ 
    ModelForm class inherits form Django's ModelForm class, set FormHelper 
    configurations using meta data, initializes localizations and more.
    """

    def __new__(cls, *args, **kwargs):
        # get new class instance using super class
        new_class = super(forms.ModelForm, cls).__new__(cls)
        
        # localize input checks of all decimal fields
        new_class._localize_fields()
        
        # return the new class      
        return new_class
    
    def __init__(self, *args, **kwargs):
        # initialize form by super class
        super(ModelForm, self).__init__(*args, **kwargs)
        
        # initialize Crispy FormHelper
        self._init_form_helper()
        
        # change required status by meta data
        if hasattr(self.Meta, 'required'):
            for field in self.Meta.required:
                self.fields[field].required = self.Meta.required[field]
            
        # set widgets by meta data (in contrast to the superclass,
        # the widgets are also set for additional fields)
        if hasattr(self.Meta, 'widgets'):
            for field in self.Meta.widgets:
                self.fields[field].widget = self.Meta.widgets[field]
                
        # set read-only flag after creation         
        if hasattr(self.Meta, 'readonly_after_creation'):
            instance = kwargs['instance']
            if instance:
                for field in self.Meta.readonly_after_creation:
                    self.fields[field].widget.attrs['readonly'] = True
    
    def _localize_fields(self):
        """ Localizes input checks of all decimal fields. """
        for field in self.base_fields.values():
            if isinstance(field, forms.DecimalField):
                field.localize = True
                field.widget.is_localized = True
                
    def _init_form_helper(self):
        """ Initializes Crispy FormHelper. """
        self._config_form_helper()   
        self._generate_layout()
                
    def _config_form_helper(self):
        """ Sets general configurations of Crispy FormHelper. """
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8' 
        
    def _generate_layout(self):
        """ Generates the layout for Crispy FormHelper. """
        fields = []
        for field in self.Meta.fields:
            fields.append(self._get_field_object(field))
        fields.append(self._get_form_actions())
        self.helper.layout = Layout(*fields)
            
    def _get_field_object(self, field):
        """ Returns the field object specified by meta data. """
        # check for appended text
        if hasattr(self.Meta, 'appended_texts') and \
                field in self.Meta.appended_texts:
            appended_text = self.Meta.appended_texts[field]
        else:
            appended_text = None
            
        # check for prepended text
        if hasattr(self.Meta, 'prepended_texts') and \
                field in self.Meta.prepended_texts:
            prepended_text = self.Meta.prepended_texts[field]
        else:
            prepended_text = None
        
        # return object
        if appended_text or prepended_text:
            return PrependedAppendedText(field, prepended_text, 
                                         appended_text)
        else:
            return Field(field)
    
    def _get_form_actions(self):
        """ Returns the FormActions object. """
        if hasattr(self.Meta, 'submit'):
            return FormActions(Submit(**self.Meta.submit))