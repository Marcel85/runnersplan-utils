# -*- coding: utf-8 -*-
from django import forms
from django.template import Context, loader 
from runnersplan.utils.widgets.base_widget import BaseWidget

class ShoesselectionWidget(BaseWidget, forms.TextInput):
    """ Shoes Selection widget """
    
    def __init__(self, attrs={}):
        # hide basic input field
        attrs.update({'style' : 'display:none'})
        
        # call init method of super class
        super(ShoesselectionWidget, self).__init__(attrs)
    
    def render(self, name, value, attrs=None):
        """ Returns the Shoes Selection widget rendered as HTML with general and added code. """
        
        # prepare rendering of added HTML
        template = loader.get_template('utils/widgets/shoesselection.html') 
        context = Context({'input' : attrs['id']})
        
        # return general HTML and added HTML
        return super(ShoesselectionWidget, self).render(name, value, attrs) + \
            '\n' + template.render(context)