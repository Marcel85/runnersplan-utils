# -*- coding: utf-8 -*-
from django import forms
from django.template import Context, loader 
from runnersplan.utils.widgets.base_widget import BaseWidget

class SummernoteWidget(BaseWidget, forms.Textarea):
    """ Summernote widget """
    
    def __init__(self, attrs={}):
        # hide basic input field
        attrs.update({'style' : 'display:none'})
        
        # call init method of super class
        super(SummernoteWidget, self).__init__(attrs)
    
    def render(self, name, value, attrs=None):
        """ Returns the Summernote widget rendered as HTML with general and added code. """
        
        # prepare rendering of added HTML
        template = loader.get_template('utils/widgets/summernote.html') 
        context = Context({'textarea' : attrs['id']})
        
        # return general HTML and added HTML
        return super(SummernoteWidget, self).render(name, value, attrs) + \
            '\n' + template.render(context)