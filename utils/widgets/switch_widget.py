# -*- coding: utf-8 -*-
from django import forms
from runnersplan.utils.widgets.base_widget import BaseWidget

class SwitchWidget(BaseWidget, forms.CheckboxInput):
    """ Switch widget """
    
    def __init__(self, attrs={}, check_test=None, size=None, 
                 on_label='an', off_label='aus', text_label=None,
                 on_color='primary', off_color='default'):
        
        # add data-attributes which specified the switch widget 
        if size: attrs.update({'class' : 'switch-' + size})
        if on_label: attrs.update({'data-on-label' : on_label})
        if off_label: attrs.update({'data-off-label' : off_label})
        if text_label: attrs.update({'data-text-label' : text_label})
        if on_color: attrs.update({'data-on' : on_color})
        if off_color: attrs.update({'data-off' : off_color})
        
        # call init method of super class
        super(SwitchWidget, self).__init__(attrs, check_test)