# -*- coding: utf-8 -*-
from django import forms
from django.template import Context, loader 
from runnersplan.utils.widgets.base_widget import BaseWidget

class ColorpaletteWidget(BaseWidget, forms.TextInput):
    """ Colorpalette widget """
    
    def __init__(self, attrs={}):
        # add data-toogle attribute
        attrs.update({'data-toogle' : 'dropdown'})
        
        # call init method of super class
        super(ColorpaletteWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        """ Returns the Colorpalette widget rendered as HTML with general and added code """
        
        # prepare rendering of added HTML
        template = loader.get_template('utils/widgets/colorpalette.html') 
        context = Context({'colorpalette_id' : attrs['id'] + '_colorpalette',
                           'text_input' : attrs['id']})
        
        # return general HTML and added HTML
        return super(ColorpaletteWidget, self).render(name, value, attrs) + \
            '\n' + template.render(context)