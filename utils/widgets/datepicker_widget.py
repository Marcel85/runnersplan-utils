# -*- coding: utf-8 -*-
from django import forms
from runnersplan.utils.widgets.base_widget import BaseWidget

class DatepickerWidget(BaseWidget, forms.DateInput):
    """ Datepicker widget """
    
    def __init__(self, attrs={}, format=None, 
                 date_format='dd.mm.yyyy', 
                 today_button='linked', 
                 language='de',
                 calendar_weeks=True,
                 autoclose=True,
                 today_highlight=True ):
        
        # add data-attributes which specified the datepicker 
        attrs.update({'data-format' : date_format,
                      'data-today-button' : today_button,
                      'data-language' : language,
                      'data-calendar-weeks' : calendar_weeks,
                      'data-autoclose' : autoclose,
                      'data-today-highlight' : today_highlight})
        
        # call init method of super class
        super(DatepickerWidget, self).__init__(attrs, format)
