# -*- coding: utf-8 -*-
class BaseWidget(object):
    """ Base widget """
    
    @property
    def attrs(self):
        if not hasattr(self, '_attrs'): 
            self._attrs = None
        return self._distinct_class(self._attrs)

    @attrs.setter
    def attrs(self, value):
        self._attrs = self._distinct_class(value)
            
    def _distinct_class(self, value):
        if 'class' in value:
            value['class'] = ' '.join(list(set(value['class'].split())))
        return value