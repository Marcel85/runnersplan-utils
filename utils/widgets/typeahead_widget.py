# -*- coding: utf-8 -*-
from django import forms
from django.template import Context, loader 
from jsmin import jsmin
from runnersplan.utils.widgets.base_widget import BaseWidget

class TypeaheadWidget(BaseWidget, forms.TextInput):
    """ Typeahead widget """
    
    def __init__(self, def_template, attrs={}):
        
        # rendering JavaScript code
        template = loader.get_template(def_template) 
        context = Context()
        js_code = self._escape(jsmin(template.render(context)))

        # add data-def attribute
        attrs.update({'data-def' : js_code})
        
        # call init method of super class
        super(TypeaheadWidget, self).__init__(attrs)
        
    def _escape(self, js_code):
        """ Escapes the quotes and backslashes in a string of JavaScript code """
        return js_code.replace('\\','\\\\').replace('"', '\\"')