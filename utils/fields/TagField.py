# -*- coding: utf-8 -*-
from django import forms
from django.db import models
import re

class TagFormField(forms.CharField):
    """ Form field class of a tag. """
    
    # definitions
    default_error_messages = {
        'invalid': u'Tags dürfen nur Buchstaben (inkl. Umlaute), Ziffern, Bindestriche und Unterstriche enthalten.',
    }

    def clean(self, value):
        """ Validate the value. """
        # return an empty string if the field is empty and not required
        if value == '' and not self.required:
            return u''
        
        # check and return the format of the field value
        value = super(TagFormField, self).clean(value)
        if not re.match(r'([\w-]*)$', value, re.UNICODE):
            raise forms.ValidationError(self.error_messages['invalid'])
        return value


class TagField(models.CharField):
    """ Model field class of a tag. """
 
    # definitions
    description = "Tag"
        
    def get_prep_value(self, value):
        """ Returns the query value. """
        try:
            return value
        except ValueError:
            return None

    def formfield(self, *args, **kwargs):
        """ Returns the default form field to use when this model field is displayed in a form. """
        kwargs['form_class'] = TagFormField
        return super(TagField, self).formfield(*args, **kwargs)