# -*- coding: utf-8 -*-
from django import forms

class ShoesSelectionField(forms.CharField):
    """ Form field class for the Shoes Selection widget. """
    
    # definitions
    default_error_messages = {
        'invalid': u'Schuhpaar und Distanz müssen immer paarweise angegeben werden.',
    }

    def clean(self, value):
        """ Validate the value. """
        # return an empty string if the field is empty and not required
        if value == '' and not self.required:
            return u''
        
        # check and return the format of the field value
        value = super(ShoesSelectionField, self).clean(value)
        items = ShoesSelectionField.getList(value)
        try:
            for item in items:
                if not (item[0].isdigit() and float(item[1])):
                    raise forms.ValidationError(self.error_messages['invalid'])
        except:
            raise forms.ValidationError(self.error_messages['invalid'])
        return value
    
    def getList(value):
        """ Returns the deserialized list of a shoes selection value. """
        return [item.split(':') for item in value.split('|')]
    getList = staticmethod(getList)