# -*- coding: utf-8 -*-
import re
from django import forms
from django.db import models
 
class ColorFormField(forms.IntegerField):
    """ Form field class of an hex color value. """
    
    # definitions
    default_error_messages = {
        'invalid': u'Bitte einen gültigen Farbwert eingeben, z.B. "#CC3300".',
    }
        
    def clean(self, value):
        """ Validate the value. """
        # return an empty string if the field is empty and not required
        if value == '' and not self.required:
            return u''
 
        # check the format of the field value
        if not re.match('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$', value):
            raise forms.ValidationError(self.error_messages['invalid'])
            
        # remove the first sign and convert hex to decimal
        value = int(value[1:], 16)
        super(ColorFormField, self).clean(value)
        return value
 
 
class ColorField(models.PositiveIntegerField):
    """ Model field class of an hex color value. """
 
    # definitions
    description = "Hexadezimaler Farbwert"
 
    def __init__(self, *args, **kwargs):
        """ Initialization. """
        kwargs['max_length'] = 6
        super(ColorField, self).__init__(*args, **kwargs)
     
    def to_python(self, value):
        """ Converting hex value to integer. """
        super(ColorField, self).to_python(value)
     
        try:
            string = "{0:0>6}".format(hex(value)[2:])
            return "#"+string.upper()
        
        except TypeError:
            return None
        
    def get_prep_value(self, value):
        """ Returns the query value. """
        try:
            return value
        except ValueError:
            return None

    def formfield(self, *args, **kwargs):
        """ Returns the default form field to use when this model field is displayed in a form. """
        kwargs['form_class'] = ColorFormField
        return super(ColorField, self).formfield(*args, **kwargs)